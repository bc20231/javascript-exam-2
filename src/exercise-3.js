const getNumberType = (number) => {
  // implement code here
  let type;
  if (number <= 0) {
	  type = null;
  } else if (number % 2 == 1) {
	  type = 'odd number';
  } else  if (number % 2 == 0) {
	  type = 'even number';
  }
  return type;
};

export default getNumberType;