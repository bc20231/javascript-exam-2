const getTheExactNumber = (numbers) => {
  // implement code here
  const result = numbers.filter(item => item % 3 === 0);
  return result[result.length - 1];
}

export default getTheExactNumber;